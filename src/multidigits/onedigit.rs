use embedded_hal::digital::v2::OutputPin;
use embedded_hal::blocking::delay::DelayMs;

use crate::bus::DataBus;

use super::MultiDigits;


/// High level interface for 1 digit 7-segments display.
pub struct OneDigit<B: DataBus, O: OutputPin> {
    bus: B,
    pin: O,
}


impl<B: DataBus, O: OutputPin> OneDigit<B, O> {
    pub fn new(pin: O, bus: B) -> Self {
        Self {pin, bus}
    }
}


impl<B: DataBus, O: OutputPin> MultiDigits for OneDigit<B, O> {
    fn clear(&mut self) {
        self.bus.clear();
    }

    fn display<D: DelayMs<u8>>(&mut self, number: u16, delay: &mut D) {
        self.bus.display((number % 10) as u8, &mut self.pin, delay);
    }
}

