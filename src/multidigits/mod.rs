//! A module with high level interfaces for various 7-segments based display.
//! You can create custom X digits 7-segments display interface with the [`MultiDigits`] trait.
//!
//! ## Example of usage
//! This example use a shift register with the [`ShiftRegister7Seg`][crate::bus::ShiftRegister7Seg]
//! structure and the [`FourDigits`] structure.
//!
//! ```ignore
//! // ... Declaration of other pins and delay
//! let digit1_pin = ...; // Declaration of the pin for digit 1
//! let digit2_pin = ...; // Declaration of the pin for digit 2
//! let digit3_pin = ...; // Declaration of the pin for digit 3
//! let digit4_pin = ...; // Declaration of the pin for digit 4
//! let latch_pin = ...;  // Declaration of latch pin
//! let clock_pin = ...;  // Declaration of clock pin
//! let data_pin = ...;  // Declaration of data pin
//!
//! let shift_register = ShiftRegister7Seg::new(clock_pin, data_pin, latch_pin, 10);
//! let mut four_digits = FourDigits::new(
//!     digit1_pin,
//!     digit2_pin,
//!     digit3_pin,
//!     digit4_pin,
//!     shift_register
//! );
//!
//! loop {
//!     for i in 0..2000 {
//!         four_digits.multiplexed_display(i, &mut delay, 500);
//!     }
//! }
//! ```
//!
//! ## Create custom interface
//!
//! Here, we re-implement partialy the  [`FourDigits`] structure.
//!
//! ```
//! use embedded_hal::digital::v2::OutputPin;
//! use seven_segments_driver::multidigits::MultiDigits;
//! use seven_segments_driver::bus::DataBus;
//!
//! pub struct FourDigits<B, O1, O2, O3, O4> {
//!     bus: B,
//!     digit1: O1,
//!     digit2: O2,
//!     digit3: O3,
//!     digit4: O4,
//! }
//!
//! impl<B, O1, O2, O3, O4> MultiDigits for FourDigits<B, O1, O2, O3, O4>
//! where
//!     B: DataBus,
//!     O1: OutputPin,
//!     O2: OutputPin,
//!     O3: OutputPin,
//!     O4: OutputPin,
//! {
//!     fn clear(&mut self) {
//!         self.bus.clear();
//!     }
//!
//!     fn display<D: DelayMs<u8>>(&mut self, number: u16, delay: &mut D) {
//!        let d1 = ((number / 1000) % 10) as u8;
//!        let d2 = ((number / 100) % 10) as u8;
//!        let d3 = ((number / 10) % 10) as u8;
//!        let d4 = (number % 10) as u8;
//!
//!        // display digit1
//!        self.digit2.set_low().ok();
//!        self.digit3.set_low().ok();
//!        self.digit4.set_low().ok();
//!        self.bus.display(d1, &mut self.digit1, delay);
//! 
//!        // display digit2
//!        self.digit1.set_low().ok();
//!        self.digit3.set_low().ok();
//!        self.digit4.set_low().ok();
//!        self.bus.display(d2, &mut self.digit2, delay);
//!
//!        // display digit3
//!        self.digit1.set_low().ok();
//!        self.digit2.set_low().ok();
//!        self.digit4.set_low().ok();
//!        self.bus.display(d3, &mut self.digit3, delay);
//!
//!        // display digit4
//!        self.digit1.set_low().ok();
//!        self.digit2.set_low().ok();
//!        self.digit3.set_low().ok();
//!        self.bus.display(d4, &mut self.digit4, delay);
//!    }
//! }
//! ```
use embedded_hal::blocking::delay::DelayMs;


mod onedigit;
mod fourdigits;

pub use onedigit::OneDigit;
pub use fourdigits::FourDigits;


/// A trait to create high level interface to use multi-digits 7-segments display.
pub trait MultiDigits {
    fn clear(&mut self);
    fn display<D: DelayMs<u8>>(&mut self, number: u16, delay: &mut D);
}

