use embedded_hal::digital::v2::OutputPin;
use embedded_hal::blocking::delay::DelayMs;

use crate::bus::DataBus;

use super::MultiDigits;


/// High level interface for 4 digits 7-segments display.
pub struct FourDigits<B, O1, O2, O3, O4> {
    bus: B,
    digit1: O1,
    digit2: O2,
    digit3: O3,
    digit4: O4,
}


impl<B, O1, O2, O3, O4> FourDigits<B, O1, O2, O3, O4>
where
    B: DataBus,
    O1: OutputPin,
    O2: OutputPin,
    O3: OutputPin,
    O4: OutputPin,
{
    pub fn new(digit1: O1, digit2: O2, digit3: O3, digit4: O4, bus: B) -> Self {
        Self {digit1, digit2, digit3, digit4, bus}
    }

    /// Multiplexed display during a time specified by `during` argument in miliseconds.
    /// This method block the code execution, used the non-blocking API to don't block execution.
    ///
    /// ### Example
    ///
    /// ```
    /// use seven_segments_driver::{bus::SevenSegments, multidigits::FourDigits};
    ///
    /// ...  // initializations of pins and delay
    /// let seven_segments = SevenSegments::new(seg_a, seg_b, seg_c, seg_d, seg_e, seg_f, seg_g,
    /// seg_dp, 15);
    /// let four_digits = FourDigits::new(digit1, digit2, digit3, digit4, seven_segments);
    ///
    /// four_digits.multiplexed_display(2022u16, &mut delay, 5000);  // display `2022` during 5 seconds
    /// ```
    pub fn multiplexed_display<D: DelayMs<u8>>(&mut self, number: u16, delay: &mut D, during: u16) {
        let time = during / (4 * self.bus.period() as u16);
        for _ in 0..time {
            self.display(number, delay);
        }
    }
}


impl<B, O1, O2, O3, O4> MultiDigits for FourDigits<B, O1, O2, O3, O4>
where
    B: DataBus,
    O1: OutputPin,
    O2: OutputPin,
    O3: OutputPin,
    O4: OutputPin,
{
    fn clear(&mut self) {
        self.bus.clear();
    }

    fn display<D: DelayMs<u8>>(&mut self, number: u16, delay: &mut D) {
        let d1 = ((number / 1000) % 10) as u8;
        let d2 = ((number / 100) % 10) as u8;
        let d3 = ((number / 10) % 10) as u8;
        let d4 = (number % 10) as u8;

        // display digit1
        self.digit2.set_low().ok();
        self.digit3.set_low().ok();
        self.digit4.set_low().ok();
        self.bus.display(d1, &mut self.digit1, delay);

        // display digit2
        self.digit1.set_low().ok();
        self.digit3.set_low().ok();
        self.digit4.set_low().ok();
        self.bus.display(d2, &mut self.digit2, delay);

        // display digit3
        self.digit1.set_low().ok();
        self.digit2.set_low().ok();
        self.digit4.set_low().ok();
        self.bus.display(d3, &mut self.digit3, delay);

        // display digit4
        self.digit1.set_low().ok();
        self.digit2.set_low().ok();
        self.digit3.set_low().ok();
        self.bus.display(d4, &mut self.digit4, delay);
    }
}

