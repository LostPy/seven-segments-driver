use embedded_hal::digital::v2::OutputPin;

use crate::error::Error;

use super::DataBus;


const BINARY_DIGITS: [u8; 8] = [
    0b1000_0000,
    0b0100_0000,
    0b0010_0000,
    0b0001_0000,
    0b0000_1000,
    0b0000_0100,
    0b0000_0010,
    0b0000_0001,
];


/// A [`DataBus`] for a shift register connect to a 7-segments.
/// Pins Q0 to Q6 of the shift register must be connected to the pins of 
/// segments A to G respectively and Q7 to pin DP.
pub struct ShiftRegister7Seg<CLOCK, DATA, LATCH> {
    clock_pin: CLOCK,
    data_pin: DATA,
    latch_pin: LATCH,
    period: u8,
}


impl<CLOCK, DATA, LATCH> ShiftRegister7Seg<CLOCK, DATA, LATCH>
where
    CLOCK: OutputPin,
    DATA: OutputPin,
    LATCH: OutputPin,
{
    pub fn new(clock_pin: CLOCK, data_pin: DATA, latch_pin: LATCH, period: u8) -> Self {
        Self {clock_pin, data_pin, latch_pin, period}
    }

    /// Send data to the shift register (equivalent to the `shiftOut` function of Arduino).
    /// Returns a Error if it can't change state of a pin (latch, clock or data).
    fn send_data(&mut self, data: u8) -> Result<(), Error>{
        if self.latch_pin.set_low().is_ok() {
            for digit in BINARY_DIGITS {
                if match self.clock_pin.set_low() {
                    Ok(_) => match data & digit {
                        0 => self.data_pin.set_high().ok(),
                        _ => self.data_pin.set_low().ok(),
                    },
                    Err(_) => return Err(Error::DataNotSend),
                }.is_none() {
                    return Err(Error::DataNotSend);
                }
                self.clock_pin.set_high().ok();
            }
            self.latch_pin.set_high().ok();
        }
        Err(Error::DataNotSend)
    }
}

impl<CLOCK, DATA, LATCH> DataBus for ShiftRegister7Seg<CLOCK, DATA, LATCH>
where
    CLOCK: OutputPin,
    DATA: OutputPin,
    LATCH: OutputPin,
{
    fn period(&self) -> u8 {
        self.period
    }

    fn clear(&mut self) {
        self.send_data(0b0000_0000).ok();
    }

    fn display_0(&mut self) {
        self.send_data(0b0011_1111).ok();
    }

    fn display_1(&mut self) {
        self.send_data(0b0000_0110).ok();
    }

    fn display_2(&mut self) {
        self.send_data(0b0101_1011).ok();
    }

    fn display_3(&mut self) {
        self.send_data(0b0100_1111).ok();
    }

    fn display_4(&mut self) {
        self.send_data(0b0110_0110).ok();
    }

    fn display_5(&mut self) {
        self.send_data(0b0110_1101).ok();
    }

    fn display_6(&mut self) {
        self.send_data(0b0111_1101).ok();
    }

    fn display_7(&mut self) {
        self.send_data(0b0000_0111).ok();
    }

    fn display_8(&mut self) {
        self.send_data(0b0111_1111).ok();
    }

    fn display_9(&mut self) {
        self.send_data(0b0110_1111).ok();
    }
}
