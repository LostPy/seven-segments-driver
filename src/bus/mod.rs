//! Module to defines methods to send data to a 7-segments.
//!
//! The [`DataBus`] trait allow to customize the usage of a 7-segments, for example, this crate
//! provide 2 way to use a 7-segments:
//!
//!  * [`SevenSegments`] structure: Use a 7-segments directly connected to the pins of MCU.
//!  * [`ShiftRegister7Seg`] structure: Use a 7-segments with a shift register, the pins A-G
//!  are connected at outputs of shift register (Q0/A to Q6/G).
//!
//!  You can create an other way with a structure which implement the [`DataBus`] trait.
//!
//!  ## Examples
//!
//!  ### Usage of [`SevenSegments`]
//!
//!  ```ignore
//!  // ... declaration of pins and delay
//!
//!  let seven_seg = SevenSegments::new(seg_a, seg_b, seg_c, seg_d, seg_e, seg_f, seg_g, 10);
//!  
//!  loop {
//!     seven_seg.display(6, &mut digit_pin, &mut delay);
//!  }
//!  ```
//!
//!  ### Usage of [`ShiftRegister7Seg`]
//!
//!  ```ignore
//!  // ... declaration of other pins and delay
//!  let latch_pin = ...;
//!  let clock_pin = ...;
//!  let data_pin = ...;
//!
//!  let shift_register = ShiftRegister7Seg::new(clock_pin, data_pin, latch_pin, 10);
//!  
//!  loop {
//!     shift_register.display(6, &mut digit_pin, &mut delay);
//!  }
//!
//!  ### Custom structure
//!
//!  ```ignore
//!  use seven_segments_driver::bus::DataBus;
//!
//!  struct My7Segments {
//!     period: u8,
//!     ... // fields
//!  }
//!
//!  impl DataBus for My7Segments {
//!     fn period(&self) -> u8 {
//!         self.period
//!     }
//!
//!     fn display_0(&mut self) {
//!         // here code to display `0`
//!     }
//!
//!     fn display_1(&mut self) {
//!         // here code to display `1`
//!     }
//!
//!     // ... And other functions from `0` to `9`.
//!  }
//!  ```
//!  It's not necessary to implement [`display`][DataBus::display], a default implementation used
//!  otther method to display a number.

use embedded_hal::digital::v2::OutputPin;
use embedded_hal::blocking::delay::DelayMs;


mod seven_segments;
mod shift_register;

pub use seven_segments::SevenSegments;
pub use shift_register::ShiftRegister7Seg;


/// Methods to send data to display on a 7-segments.
pub trait DataBus {
    /// Returns the period of display (time).
    /// Use a small number (in miliseconds) for a multiplexer display.
    fn period(&self) -> u8;

    /// Clear the display.
    fn clear(&mut self);

    /// Set 7-segments states to display `0`.
    fn display_0(&mut self);

    /// Set 7-segments states to display `1`.
    fn display_1(&mut self);

    /// Set 7-segments states to display `2`.
    fn display_2(&mut self);

    /// Set 7-segments states to display `3`.
    fn display_3(&mut self);

    /// Set 7-segments states to display `4`.
    fn display_4(&mut self);

    /// Set 7-segments states to display `5`.
    fn display_5(&mut self);

    /// Set 7-segments states to display `6`.
    fn display_6(&mut self);

    /// Set 7-segments states to display `7`.
    fn display_7(&mut self);

    /// Set 7-segments states to display `8`.
    fn display_8(&mut self);

    /// Set 7-segments states to display `9`.
    fn display_9(&mut self);

    /// Display a number on a OutputPin during the time given by [`self.period()`][DataBus::period].
    fn display<T: DelayMs<u8>, O: OutputPin>(&mut self, number: u8, digit_pin: &mut O, delay: &mut T) {
        if digit_pin.set_low().is_ok() {
            match number {
                0 => self.display_0(),
                1 => self.display_1(),
                2 => self.display_2(),
                3 => self.display_3(),
                4 => self.display_4(),
                5 => self.display_5(),
                6 => self.display_6(),
                7 => self.display_7(),
                8 => self.display_8(),
                9 => self.display_9(),
                _ => self.clear(),
            }
            digit_pin.set_high().ok();
            delay.delay_ms(self.period());
        }
    }
}
