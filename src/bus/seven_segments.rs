use embedded_hal::digital::v2::OutputPin;

use super::DataBus;


/// DataBus to send data to a 7-segments directly connect to pins of MCU.
/// Each segments is connected to a OutputPin.
pub struct SevenSegments<A, B, C, D, E, F, G> {
    segment_a: A,
    segment_b: B,
    segment_c: C,
    segment_d: D,
    segment_e: E,
    segment_f: F,
    segment_g: G,
    /// Period of multiplexer display in miliseconds.
    period: u8,
}


impl<A, B, C, D, E, F, G> SevenSegments<A, B, C, D, E, F, G>
where
    A: OutputPin,
    B: OutputPin,
    C: OutputPin,
    D: OutputPin,
    E: OutputPin,
    F: OutputPin,
    G: OutputPin,
{

    /// Create a new SevenSegments object.
    #[allow(clippy::too_many_arguments)]
    pub fn new(segment_a: A,
               segment_b: B,
               segment_c: C,
               segment_d: D,
               segment_e: E,
               segment_f: F,
               segment_g: G,
               period: u8) -> Self {
        Self {
            segment_a,
            segment_b,
            segment_c,
            segment_d,
            segment_e,
            segment_f,
            segment_g,
            period
        }
    }
}

impl<A, B, C, D, E, F, G> DataBus for SevenSegments<A, B, C, D, E, F, G>
where
    A: OutputPin,
    B: OutputPin,
    C: OutputPin,
    D: OutputPin,
    E: OutputPin,
    F: OutputPin,
    G: OutputPin,
{
    fn period(&self) -> u8 {
        self.period
    }

    fn clear(&mut self) {
        self.segment_a.set_high().ok();
        self.segment_b.set_high().ok();
        self.segment_c.set_high().ok();
        self.segment_d.set_high().ok();
        self.segment_e.set_high().ok();
        self.segment_f.set_high().ok();
        self.segment_g.set_high().ok();
    }

    fn display_0(&mut self) {
        self.segment_a.set_low().ok();
        self.segment_b.set_low().ok();
        self.segment_c.set_low().ok();
        self.segment_d.set_low().ok();
        self.segment_e.set_low().ok();
        self.segment_f.set_low().ok();
        self.segment_g.set_high().ok();
    }

    fn display_1(&mut self) {
        self.segment_a.set_high().ok();
        self.segment_b.set_low().ok();
        self.segment_c.set_low().ok();
        self.segment_d.set_high().ok();
        self.segment_e.set_high().ok();
        self.segment_f.set_high().ok();
        self.segment_g.set_high().ok();
    }

    fn display_2(&mut self) {
        self.segment_a.set_low().ok();
        self.segment_b.set_low().ok();
        self.segment_c.set_high().ok();
        self.segment_d.set_low().ok();
        self.segment_e.set_low().ok();
        self.segment_f.set_high().ok();
        self.segment_g.set_low().ok();
    }

    fn display_3(&mut self) {
        self.segment_a.set_low().ok();
        self.segment_b.set_low().ok();
        self.segment_c.set_low().ok();
        self.segment_d.set_low().ok();
        self.segment_e.set_high().ok();
        self.segment_f.set_high().ok();
        self.segment_g.set_low().ok();
    }

    fn display_4(&mut self) {
        self.segment_a.set_high().ok();
        self.segment_b.set_low().ok();
        self.segment_c.set_low().ok();
        self.segment_d.set_high().ok();
        self.segment_e.set_high().ok();
        self.segment_f.set_low().ok();
        self.segment_g.set_low().ok();
    }

    fn display_5(&mut self) {
        self.segment_a.set_low().ok();
        self.segment_b.set_high().ok();
        self.segment_c.set_low().ok();
        self.segment_d.set_low().ok();
        self.segment_e.set_high().ok();
        self.segment_f.set_low().ok();
        self.segment_g.set_low().ok();
    }

    fn display_6(&mut self) {
        self.segment_a.set_low().ok();
        self.segment_b.set_high().ok();
        self.segment_c.set_low().ok();
        self.segment_d.set_low().ok();
        self.segment_e.set_low().ok();
        self.segment_f.set_low().ok();
        self.segment_g.set_low().ok();
    }

    fn display_7(&mut self) {
        self.segment_a.set_low().ok();
        self.segment_b.set_low().ok();
        self.segment_c.set_low().ok();
        self.segment_d.set_high().ok();
        self.segment_e.set_high().ok();
        self.segment_f.set_high().ok();
        self.segment_g.set_high().ok();
    }

    fn display_8(&mut self) {
        self.segment_a.set_low().ok();
        self.segment_b.set_low().ok();
        self.segment_c.set_low().ok();
        self.segment_d.set_low().ok();
        self.segment_e.set_low().ok();
        self.segment_f.set_low().ok();
        self.segment_g.set_low().ok();
    }

    fn display_9(&mut self) {
        self.segment_a.set_low().ok();
        self.segment_b.set_low().ok();
        self.segment_c.set_low().ok();
        self.segment_d.set_low().ok();
        self.segment_e.set_high().ok();
        self.segment_f.set_low().ok();
        self.segment_g.set_low().ok();
    }
}

