# 7-segments-driver

An [`embedded-hal`][emb-hal] implementation for multi-digits 7-segments display.

## Examples

```rust
fn main() -> ! {
    ... // Initialisation of pins and delay
    let seven_seg = SevenSegments::new(a, b, c, d, e, f, g, 7);  // 7 to display each difigts during 7ms, a small number is used for multiplexed display.
    let mut four_digits_display = FourDigits::new(
        digit1,
        digit2,
        digit3,
        digit4,
        seven_seg,
    );

    let mut delay = Delay::new(cp.SYST, &rcc);

    loop {
        for i in 0..1501 {
            four_digits_display.multiplexed_display(i, &mut delay, 200);
        }
    }
}
```

## Getting Started

Add this crate in dependencies in `Cargo.toml`:

```
seven-segments-driver = { git = "https://gitlab.com/LostPy/seven-segments-driver.git" branch = "master" }
```

To quick test the crate, you can clone the repository and test examples.

> :warning: Examples are avaible for a limited number of platforms. If you have an other platform supported by [`embedded-hal`][emb-hal], the code must be partially re-writte.

 1. Clone git repo:
```
git clone https://gitlab.com/LostPy/seven-segments-driver.git
```

 2. move in an example directory

```
cd examples/blocking/sevensegments/stm32f0xx
```

 3. Run and Flash

```
cargo run --release
```

## Documentation

You can get the documentation using `cargo doc` command:

```
cargo doc --package seven-segments-driver --open
```

## Async API

The asynchrone API is not yet avaible.

## Features

Features uncheck are not yet implemented but are planned.

 - [x] Blocking API
   - [x] Trait for single 7-segments
   - [x] Trait to build custom multi digits 7-segments
   - [x] Support for a single digit 7-segments
   - [x] Support for 4 digits 7-segments
   - [x] Support for 7-segments control by shift-register
 - [ ] Async API 
   - [ ] Trait for single 7-segments
   - [ ] Trait to build custom multi digits 7-segments
   - [ ] Support for a single digit 7-segments
   - [ ] Support for 4 digits 7-segments

## Contributing

 * Additional issues and merge-requests are welcome
 * If a platform is not yet covered in examples that is supported by [`embedded-hal`][emb-hal], you can merge-request an example for this platform.

### Todo

 * Improve documentation
 * Add examples for differents platforms
 * Implement async API
 * Add test
 * Better management of `OutputPin::Error`

[emb-hal]: https://github.com/rust-embedded/embedded-hal

