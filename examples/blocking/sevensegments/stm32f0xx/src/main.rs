#![no_std]
#![no_main]

use panic_halt as _;

use cortex_m::{peripheral::Peripherals, interrupt};
use stm32f0xx_hal as hal;
use crate::hal::{pac, delay::Delay, prelude::*};
use cortex_m_rt::entry;

use seven_segments_driver::{bus::SevenSegments, multidigits::FourDigits};



#[entry]
fn main() -> ! {
    
    if let (Some(mut dp), Some(cp)) = (pac::Peripherals::take(), Peripherals::take()) {
        let mut rcc = dp.RCC.configure().sysclk(8.mhz()).freeze(&mut dp.FLASH);

        let gpiob = dp.GPIOB.split(&mut rcc);

        let digit1 = interrupt::free(|cs| gpiob.pb3.into_push_pull_output(cs));
        let digit2 = interrupt::free(|cs| gpiob.pb5.into_push_pull_output(cs));
        let digit3 = interrupt::free(|cs| gpiob.pb4.into_push_pull_output(cs));
        let digit4 = interrupt::free(|cs| gpiob.pb6.into_push_pull_output(cs));

        let a = interrupt::free(|cs| gpiob.pb1.into_push_pull_output(cs));
        let b = interrupt::free(|cs| gpiob.pb15.into_push_pull_output(cs));
        let c = interrupt::free(|cs| gpiob.pb14.into_push_pull_output(cs));
        let d = interrupt::free(|cs| gpiob.pb13.into_push_pull_output(cs));
        let e = interrupt::free(|cs| gpiob.pb12.into_push_pull_output(cs));
        let f = interrupt::free(|cs| gpiob.pb11.into_push_pull_output(cs));
        let g = interrupt::free(|cs| gpiob.pb2.into_push_pull_output(cs));
        let seven_seg = SevenSegments::new(a, b, c, d, e, f, g, 7);
        let mut four_digits_display = FourDigits::new(
            digit1,
            digit2,
            digit3,
            digit4,
            seven_seg,
        );

        let mut delay = Delay::new(cp.SYST, &rcc);

        loop {
            for i in 0..1501 {
                four_digits_display.multiplexed_display(i, &mut delay, 200);
            }
        }
    }

    loop {
        continue;
    }
}
