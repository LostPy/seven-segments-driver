#![no_std]
#![no_main]

use panic_halt as _;

use cortex_m::{peripheral::Peripherals, interrupt};
use stm32f0xx_hal as hal;
use crate::hal::{pac, delay::Delay, prelude::*};
use cortex_m_rt::entry;

use seven_segments_driver::{bus::ShiftRegister7Seg, multidigits::FourDigits};



#[entry]
fn main() -> ! {
    
    if let (Some(mut dp), Some(cp)) = (pac::Peripherals::take(), Peripherals::take()) {
        let mut rcc = dp.RCC.configure().sysclk(8.mhz()).freeze(&mut dp.FLASH);

        let gpiob = dp.GPIOB.split(&mut rcc);

        let digit1 = interrupt::free(|cs| gpiob.pb3.into_push_pull_output(cs));
        let digit2 = interrupt::free(|cs| gpiob.pb5.into_push_pull_output(cs));
        let digit3 = interrupt::free(|cs| gpiob.pb4.into_push_pull_output(cs));
        let digit4 = interrupt::free(|cs| gpiob.pb6.into_push_pull_output(cs));

        let clock = interrupt::free(|cs| gpiob.pb15.into_push_pull_output(cs));
        let latch = interrupt::free(|cs| gpiob.pb14.into_push_pull_output(cs));
        let data = interrupt::free(|cs| gpiob.pb13.into_push_pull_output(cs));
        let shift_register = ShiftRegister7Seg::new(clock, data, latch, 7);
        let mut four_digits_display = FourDigits::new(
            digit1,
            digit2,
            digit3,
            digit4,
            shift_register,
        );

        let mut delay = Delay::new(cp.SYST, &rcc);

        loop {
            for i in 0..1501 {
                four_digits_display.multiplexed_display(i, &mut delay, 200);
            }
        }
    }

    loop {
        continue;
    }
}
